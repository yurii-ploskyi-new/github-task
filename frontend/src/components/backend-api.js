import axios from 'axios'

const AXIOS = axios.create({
  timeout: 1000
});


export default {
    getEvents(username) {
        return AXIOS.get(`/api/commits/${username}`);
    }
}


