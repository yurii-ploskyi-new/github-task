package com.task.domain;

public class Event {

    private long id;

    private String type;

    private Payload payload;

    public Event() {
    }

    public Event(long id, String type, Payload payload) {
        this.id = id;
        this.type = type;
        this.payload = payload;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }
}
