package com.task.domain;

public class Commit {
    private String message;
    private String url;
    private String sha;
    private Author author;

    public Commit() {
    }

    public Commit(String message, String url, String sha, Author author) {
        this.message = message;
        this.url = url;
        this.sha = sha;
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}