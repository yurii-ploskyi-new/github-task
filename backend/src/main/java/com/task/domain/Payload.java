package com.task.domain;

import java.util.List;

public class Payload {
    private Long push_id;
    private List<Commit> commits;

    public Payload() {
    }

    public Payload(Long push_id, List<Commit> commits) {
        this.push_id = push_id;
        this.commits = commits;
    }

    public Long getPush_id() {
        return push_id;
    }

    public void setPush_id(Long push_id) {
        this.push_id = push_id;
    }

    public List<Commit> getCommits() {
        return commits;
    }

    public void setCommits(List<Commit> commits) {
        this.commits = commits;
    }
}
