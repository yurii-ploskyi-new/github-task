package com.task.domain;

import java.util.Objects;

public class CommitDto {

    private String message;
    private String url;
    private String sha;
    private String name;
    private String email;

    public CommitDto() {
    }

    public CommitDto(String message, String url, String sha, String name, String email) {
        this.message = message;
        this.url = url;
        this.sha = sha;
        this.name = name;
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommitDto commitDto = (CommitDto) o;
        return Objects.equals(message, commitDto.message) &&
                Objects.equals(url, commitDto.url) &&
                Objects.equals(sha, commitDto.sha) &&
                Objects.equals(name, commitDto.name) &&
                Objects.equals(email, commitDto.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, url, sha, name, email);
    }
}
