package com.task.service;

import com.task.domain.CommitDto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CommitService {

    List<CommitDto> getCommits(String username);

}
