package com.task.service;

import com.task.domain.Event;
import com.task.domain.CommitDto;
import com.task.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class CommitServiceImpl implements CommitService {

    private static final String GITHUB_EVENTS_URL = "https://api.github.com/users/%s/events";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<CommitDto> getCommits(String username) {
        try {
            return CommitDtoMapper.mapToCommitDto(getGithubEvents(username));
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new UserNotFoundException(e.getMessage());
            }
            throw e;
        }

    }

    private List<Event> getGithubEvents(String username) {
        return restTemplate.exchange(String.format(GITHUB_EVENTS_URL, username),
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Event>>() {
                }).getBody();
    }
}