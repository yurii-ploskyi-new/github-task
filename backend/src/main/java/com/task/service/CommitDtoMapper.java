package com.task.service;

import com.task.domain.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CommitDtoMapper {

    public static List<CommitDto> mapToCommitDto(List<Event> events) {
        return events.stream().map(CommitDtoMapper::mapToCommitDto).flatMap(List::stream).collect(Collectors.toList());
    }

    private static List<CommitDto> mapToCommitDto(Event event) {
        return mapToCommitDto(event.getPayload());
    }

    private static List<CommitDto> mapToCommitDto(Payload payload) {
        return mapCommitsToCommitDto(payload.getCommits());
    }

    private static List<CommitDto> mapCommitsToCommitDto(List<Commit> commits) {
        if (commits == null) {
            return Collections.emptyList();
        }
        return commits.stream().map(CommitDtoMapper::mapToCommitDto).collect(Collectors.toList());
    }

    private static CommitDto mapToCommitDto(Commit commit) {
        assert commit.getAuthor() != null;
        return mapToCommitDto(commit, commit.getAuthor());
    }

    private static CommitDto mapToCommitDto(Commit commit, Author author) {
        return new CommitDto(commit.getMessage(), commit.getUrl(), commit.getSha(), author.getName(), author.getEmail());
    }

}
