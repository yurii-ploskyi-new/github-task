package com.task.controller;

import com.task.domain.CommitDto;
import com.task.exception.UserNotFoundException;
import com.task.service.CommitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CommitController {

    @Autowired
    private CommitService commitService;

    @RequestMapping(path = "/commits/{username}")
    public @ResponseBody
    List<CommitDto> getCommits(@PathVariable String username) {
        return commitService.getCommits(username);
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public @ResponseBody
    String handleUserNotFoundException(UserNotFoundException e) {
        return "user was not found";
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    String handleException(Exception e) {
        return "internal server error";
    }

}