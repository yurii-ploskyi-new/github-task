package com.task.service;

import com.task.domain.*;
import com.task.exception.UserNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CommitServiceTest {

    @InjectMocks
    private CommitServiceImpl commitService;

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = RuntimeException.class)
    public void testException() {
        when(restTemplate.exchange("https://api.github.com/users/username/events", HttpMethod.GET, null, new ParameterizedTypeReference<List<Event>>() {
        })).thenThrow(new RuntimeException());
        commitService.getCommits("username");
    }

    @Test(expected = UserNotFoundException.class)
    public void testUserNotFound() {
        when(restTemplate.exchange("https://api.github.com/users/username/events", HttpMethod.GET, null, new ParameterizedTypeReference<List<Event>>() {
        })).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));
        commitService.getCommits("username");
    }

    @Test
    public void testEmptyResult() {
        ResponseEntity<List<Event>> responseEntity = new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
        when(restTemplate.exchange("https://api.github.com/users/username/events", HttpMethod.GET, null, new ParameterizedTypeReference<List<Event>>() {
        })).thenReturn(responseEntity);
        List<CommitDto> commits = commitService.getCommits("username");
        Assert.assertNotNull(commits);
        Assert.assertTrue(commits.isEmpty());
    }

    @Test
    public void testNotEmptyCommits() {
        ResponseEntity<List<Event>> responseEntity = new ResponseEntity<>(getEvents(), HttpStatus.OK);
        when(restTemplate.exchange("https://api.github.com/users/username/events", HttpMethod.GET, null, new ParameterizedTypeReference<List<Event>>() {
        })).thenReturn(responseEntity);
        List<CommitDto> commits = commitService.getCommits("username");
        Commit expectedCommit = getCommit();
        Assert.assertNotNull(commits);
        Assert.assertEquals(1, commits.size());
        CommitDto actualCommit = commits.get(0);
        Assert.assertEquals(expectedCommit.getMessage(), actualCommit.getMessage());
        Assert.assertEquals(expectedCommit.getUrl(), actualCommit.getUrl());
        Assert.assertEquals(expectedCommit.getSha(), actualCommit.getSha());
        Assert.assertEquals(expectedCommit.getAuthor().getEmail(), actualCommit.getEmail());
        Assert.assertEquals(expectedCommit.getAuthor().getName(), actualCommit.getName());
    }

    private List<Event> getEvents() {
        List<Event> events = new ArrayList<>();
        Event event = new Event();
        Payload payload = new Payload(12l, getCommits());
        event.setPayload(payload);
        events.add(event);
        return events;
    }

    private List<Commit> getCommits() {
        List<Commit> commits = new ArrayList<>();
        commits.add(getCommit());
        return commits;
    }

    private Commit getCommit() {
        return new Commit("some maesasage", "some usr", "some sha", new Author("some name", "some email"));
    }

}