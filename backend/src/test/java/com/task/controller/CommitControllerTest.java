package com.task.controller;

import com.task.domain.CommitDto;
import com.task.exception.UserNotFoundException;
import com.task.service.CommitService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CommitController.class)
public class CommitControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommitService commitService;

    @Autowired
    private CommitController commitController;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(commitController);
    }

    @Test
    public void testEmptyResult() throws Exception {
        String username = "some username";
        when(commitService.getCommits(username)).thenReturn(Collections.emptyList());
        this.mockMvc.perform(get("/api/commits/" + username)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(is("[]")));
    }

    @Test
    public void testCommits() throws Exception {
        String username = "some username";
        CommitDto commitDto = getCommitDto();
        when(commitService.getCommits(username)).thenReturn(getCommits());
        this.mockMvc.perform(get("/api/commits/" + username)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString(commitDto.getMessage())))
                .andExpect(content().string(containsString(commitDto.getUrl())))
                .andExpect(content().string(containsString(commitDto.getSha())))
                .andExpect(content().string(containsString(commitDto.getName())))
                .andExpect(content().string(containsString(commitDto.getEmail())));
    }

    @Test
    public void testUsernameWasNotFound() throws Exception {
        String username = "some username";
        when(commitService.getCommits(username)).thenThrow(UserNotFoundException.class);
        this.mockMvc.perform(get("/api/commits/" + username)).andDo(print()).andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    public void testInternalError() throws Exception {
        String username = "some username";
        when(commitService.getCommits(username)).thenThrow(RuntimeException.class);
        this.mockMvc.perform(get("/api/commits/" + username)).andDo(print()).andExpect(status().is(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }

    private List<CommitDto> getCommits() {
        List<CommitDto> commitDtos = new ArrayList<>();
        CommitDto commitDto = getCommitDto();
        commitDtos.add(commitDto);
        return commitDtos;
    }

    private CommitDto getCommitDto() {
        return new CommitDto("some unique message", "some url", "ssh", "some name", "some email");
    }

}